package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import model.Animal;
import util.DatabaseUtil;

public class MainController implements Initializable {

	@FXML
	private ListView<String> mainListView;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populateListView();

	}

	public void populateListView() {
		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setup();
		dbUtil.startTransaction();
		ObservableList<String> names = FXCollections.observableArrayList();

		for (Animal animal : dbUtil.getAllAnimals()) {
			String appName = animal.getAppointments().get(0).getType();
			names.add("Animal name: " + animal.getName() + " has appointment: " + appName);
		}
		mainListView.setItems(names);
		mainListView.refresh();
		dbUtil.stop();
	}

}
